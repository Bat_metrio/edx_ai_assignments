# edX ML1 assignment
# Perceptron algorithm
import sys
import numpy as np
import matplotlib.pyplot as plt
import csv


def sign(nb):
    if nb > 0:
        return 1
    return -1


def conv(w_prev, w, eps):
    return (abs(w - w_prev) < eps).all()


def format_(l, n=1):
    l = l.tolist()
    return l[n:] + l[:n]


input_filename, output_filename = sys.argv[1:]
data = np.genfromtxt(input_filename, delimiter=',', names=['x', 'y', 'label'])

colors = {-1.0: 'b', 1.0: 'r'}

feat = np.array(zip(data['x'], data['y']))
feat = np.concatenate((np.ones((len(data), 1)), feat), axis=1)

# init
w_prev = np.zeros((1, 3))
w = w_prev + data['label'][0] * feat[0]
eps = 1e-3
w_out = [w_prev, w]
while not conv(w_prev, w, eps):
    print '***'
    w_prev = w.copy()
    for (i, X) in enumerate(feat):
        f = sign(np.dot(w, X.T))
        # print 'f: ' + str(f)
        # print 'w: ' + str(w)
        # print 'yf: ' + str(data['label'][i] * f)
        if data['label'][i] * f <= 0:
            w += data['label'][i] * X
            w_out.append(w)
            # print 'w: ' + str(w)
# import pdb; pdb.set_trace()
x = np.arange(0, 16, 0.1)
y = (w[0][0] + w[0][1] * x) / (-w[0][2])
fig = plt.figure()
plt.plot(x, y)

plt.grid()
plt.scatter(data['x'], data['y'], color=[colors[l] for l in data['label']])
plt.show()

# with open(output_filename, 'w') as f:
#     [f.write([str(weight) ] + '\n') for weights in w_out]
w_out = [format_(w_[0]) for w_ in w_out]

with open(output_filename, "w") as output:
    writer = csv.writer(output, lineterminator='\n')
    writer.writerows(w_out)
