#! /usr/bin/python2.7
# -*-coding:Utf-8 -*

import pandas as pd
import numpy as np
# import ipdb
import string
from os import walk
import os
import glob
from bs4 import BeautifulSoup as BS
import re
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import SGDClassifier
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV



f = []
pos_txt_files = glob.glob(os.path.join(os.getcwd(), 'aclImdb/train/pos/*.txt'))
neg_txt_files = glob.glob(os.path.join(os.getcwd(), 'aclImdb/train/neg/*.txt'))
pos_l = []

# tokenizing, removing stop_words
with open('stopwords.en.txt', 'r') as f:
    stop_words = f.read().splitlines()

for i, f in enumerate(pos_txt_files):
    with open(f, 'r') as t:
        pos_l.append([i, t.read(), 1])
nb_pos = len(pos_l)
neg_l = []

for i, f in enumerate(neg_txt_files):
    with open(f, 'r') as t:
        neg_l.append([nb_pos + i, t.read(), 0])
df = pd.DataFrame(pos_l + neg_l, columns=['row_number', 'text', 'polarity'])


# Remove HTML tags and numbers
def preprocess(df):
    df['text'] = df['text'].apply(lambda x: re.sub("[^a-zA-Z]", " ", BS(x).get_text()))
    return df

df = preprocess(df)
docs = df['text']


unigram_text_clf = Pipeline([('vect', CountVectorizer(analyzer='word', ngram_range=(1, 1), stop_words=stop_words, strip_accents='unicode', decode_error='ignore')),
                             ('clf', SGDClassifier(loss='hinge', penalty='l1',
                                                  random_state=42)),
                             ])


parameters = {'vect__max_features': [ 5000, 6000, 8000, 10000],
                'vect__max_df': [0.5, 0.8],
                # 'vect__min_df': [0.3, 0.5],
               'clf__alpha': ( 1e-3, 1e-4),
               'clf__n_iter':[2, 5, 8]}
gs_clf = GridSearchCV(unigram_text_clf, parameters, n_jobs=-1)
gs_clf = gs_clf.fit(docs, df['polarity'])


# Get and transform new data to predict polarity on
test_pos = []
pos_test_txt_files = glob.glob(os.path.join(
    os.getcwd(), 'aclImdb/test/pos/*.txt'))
for i, f in enumerate(pos_test_txt_files):
    with open(f, 'r') as t:
        test_pos.append([i, t.read(), 1])


test_neg = []
neg_test_txt_files = glob.glob(os.path.join(
    os.getcwd(), 'aclImdb/test/neg/*.txt'))
for i, f in enumerate(neg_test_txt_files):
    with open(f, 'r') as t:
        test_neg.append([len(test_pos) + i, t.read(), 1])
df_test = pd.DataFrame(test_pos + test_neg,
                       columns=['row_number', 'text', 'polarity'])

df_test = preprocess(df_test)
test_docs = df_test['text']

# Unigram test
unigram_prediction = gs_clf.predict(test_docs)
unigram_score = np.mean(unigram_prediction == df_test['polarity'])


# -----
#With GridSearch
# predicted = gs_clf.best_estimator_.predict(df_test['text'])
# score = np.mean(predicted == df_test['polarity'])
# -----
# # # test file
# test_filename = '../resource/asnlib/public/imdb_te.csv'
# test = []
# test_txt_files = glob.glob(os.path.join(os.getcwd(), test_filename))
# for i, f in enumerate(test_txt_files):
#     with open(f, 'r') as t:
#         test.append([i, t.read(), 1])
# df_test = pd.DataFrame(test, columns=['row_number', 'text', 'polarity'])
#
# X_test = tf_vectorizer.transform(pd.Series(df_test.loc[:,'text']))
#
# Y_predicted =  clf.predict(X_test)
# df_test['predicted'] = Y_predicted
# pd.Series(Y_predicted).to_csv('unigram.output.txt', header=False, index=False)


# Bigram classification
bigram_text_clf = Pipeline([('vect', CountVectorizer(analyzer='word', ngram_range=(1, 2), stop_words=stop_words, strip_accents='unicode', decode_error='ignore')),
                             ('clf', SGDClassifier(loss='hinge', penalty='l1',
                                                  random_state=42)),
                             ])


bigram_clf = GridSearchCV(bigram_text_clf, parameters, n_jobs=-1)
# unigram_text_clf = unigram_text_clf.fit(docs, df['polarity'])
bigram_clf = bigram_clf.fit(docs, df['polarity'])
bigram_predicted = bigram_clf.best_estimator_.predict(df_test['text'])
bigram_score = np.mean(bigram_predicted == df_test['polarity'])
