# edX ML3 assignment
# Classifier algorithm
import sys
import numpy as np
import matplotlib.pyplot as plt
import csv
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import LogisticRegression
from sklearn import svm
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier

def c_plot(clf, xx, yy):

    Z = best_clf.predict(np.c_[xx.ravel(), yy.ravel()])

    # Put the result into a color plot
    Z = Z.reshape(xx.shape)
    plt.contourf(xx, yy, Z, cmap=plt.cm.coolwarm, alpha=0.8)

    # Plot also the training points
    # plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap=plt.cm.coolwarm)
    # plt.xlabel('Sepal length')
    # plt.ylabel('Sepal width')
    plt.xlim(xx.min(), xx.max())
    plt.ylim(yy.min(), yy.max())
    plt.xticks(())
    plt.yticks(())
    return plt.show()


input_filename = 'input3.csv'
colors = {0.0: 'b', 1.0: 'r'}
data = np.genfromtxt(input_filename, delimiter=',', names=True)
results = []

plt.grid()
plt.scatter(data['A'], data['B'], color=[colors[l] for l in data['label']])


X = np.array((data['A'], data['B'])).T
y = np.array((data['label']))

# splitting data 60% training, 40% test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4, random_state=42)


# create a mesh to plot in
h = 0.02
x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                     np.arange(y_min, y_max, h))

# Linear kernel parameters
C = [0.1, 0.5, 1, 5, 10, 50, 100]

parameters = {'C': C}
svr = svm.SVC(kernel='linear')
clf = GridSearchCV(svr, parameters, cv=5, scoring='accuracy')
clf.fit(X_train, y_train)

# best score on training data
best_training_score = clf.best_score_

# now take best estimator with C parameter and fit on test data, get record test score
best_clf = clf.best_estimator_
test_score = best_clf.score(X_test, y_test)

results.append(['svm_linear', best_training_score, test_score])
#
# c_plot(best_clf, xx, yy)

# Polynomial kernel parameters
C = [0.1, 1, 3]; degree = [4, 5, 6]; gamma = [0.1, 1]

parameters = {'C': C, 'degree': degree, 'gamma': gamma}
svr = svm.SVC(kernel='poly')
clf = GridSearchCV(svr, parameters, cv=5, scoring='accuracy')
clf.fit(X_train, y_train)

# best score on training data
best_training_score = clf.best_score_

# now take best estimator with C parameter and fit on test data, get record test score
best_clf = clf.best_estimator_
test_score = best_clf.score(X_test, y_test)

results.append(['svm_polynomial', best_training_score, test_score])
#
# c_plot(best_clf, xx, yy)

# RBF kernel parameters
C = [0.1, 0.5, 1, 5, 10, 50, 100]; gamma = [0.1, 0.5, 1, 3, 6, 10]

parameters = {'C': C, 'gamma': gamma}
svr = svm.SVC(kernel='rbf')
clf = GridSearchCV(svr, parameters, cv=5, scoring='accuracy')
clf.fit(X_train, y_train)

# best score on training data
best_training_score = clf.best_score_

# now take best estimator with C parameter and fit on test data, get record test score
best_clf = clf.best_estimator_
test_score = best_clf.score(X_test, y_test)

results.append(['svm_rbf', best_training_score, test_score])

# c_plot(best_clf, xx, yy)

# -------------------------
# Logistic regression
C = [0.1, 0.5, 1, 5, 10, 50, 100]

parameters = {'C': C}
log = LogisticRegression()
clf = GridSearchCV(log, parameters, cv=5, scoring='accuracy')
clf.fit(X_train, y_train)

# best score on training data
best_training_score = clf.best_score_

# now take best estimator with C parameter and fit on test data, get record test score
best_clf = clf.best_estimator_
test_score = best_clf.score(X_test, y_test)

results.append(['logistic', best_training_score, test_score])

# c_plot(best_clf, xx, yy)


# -------------------------
# KNN
n_neighbors = range(1, 51); leaf_size = range(5, 61, 5)

parameters = {'n_neighbors': n_neighbors, 'leaf_size': leaf_size}
knn = KNeighborsClassifier()
clf = GridSearchCV(knn, parameters, cv=5, scoring='accuracy')
clf.fit(X_train, y_train)

# best score on training data
best_training_score = clf.best_score_

# now take best estimator with C parameter and fit on test data, get record test score
best_clf = clf.best_estimator_
test_score = best_clf.score(X_test, y_test)

results.append(['knn', best_training_score, test_score])

# c_plot(best_clf, xx, yy)


# -------------------------
# Decision trees
max_depth = range(1, 51); min_samples_split = range(2, 11)

parameters = {'max_depth': max_depth, 'min_samples_split': min_samples_split}
dtc = DecisionTreeClassifier()
clf = GridSearchCV(dtc, parameters, cv=5, scoring='accuracy')
clf.fit(X_train, y_train)

# best score on training data
best_training_score = clf.best_score_

# now take best estimator with C parameter and fit on test data, get record test score
best_clf = clf.best_estimator_
test_score = best_clf.score(X_test, y_test)

results.append(['decision_tree', best_training_score, test_score])

# c_plot(best_clf, xx, yy)

# -------------------------
# Random Forest
max_depth = range(1, 51); min_samples_split = range(2, 11)

parameters = {'max_depth': max_depth, 'min_samples_split': min_samples_split}
rfc = RandomForestClassifier()
clf = GridSearchCV(rfc, parameters, cv=5, scoring='accuracy')
clf.fit(X_train, y_train)

# best score on training data
best_training_score = clf.best_score_

# now take best estimator with C parameter and fit on test data, get record test score
best_clf = clf.best_estimator_
test_score = best_clf.score(X_test, y_test)

results.append(['random_forest', best_training_score, test_score])

c_plot(best_clf, xx, yy)


with open('output3.csv', "w") as output:
    writer = csv.writer(output, lineterminator='\n')
    writer.writerows(results)
