import sys
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import csv


def scale(column):
    return (column - column.mean()) / column.std()


def conv(B_prev, B, eps):
    return abs(B - B_prev).all() < eps


def grad_descent(a, iter, X, y, B):
    # eps = 1e-1
    # B_prev = B + eps + 1
    for i in xrange(iter):
        # B_prev = B
        s = np.dot((np.dot(B, X_red.T).T - y_red).T, X_red)
        Bc = B.copy()
        B = Bc - a * s / n
    return ([a, i] + np.ravel(B).tolist())


input_filename, output_filename = sys.argv[1:]

data = np.genfromtxt(input_filename, delimiter=',', names=[
                     'years', 'weight', 'height'])

features = ['years', 'weight']

data_red = np.zeros((len(data), len(features)))
for i, f in enumerate(features):
    data_red[:, i] = scale(data[f])


y_red = data['height']
y_red.shape = (len(data), 1)

# Add intercept to data matrix
X_red = np.append(np.ones((len(data), 1)), data_red, 1)

eps = 1e-1
rates = [0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1, 5, 10]
# Init Betas
B_init = np.zeros((1, len(features) + 1))
B = np.zeros((1, len(features) + 1))
B_prev = B + eps + 1

n = len(data)


B_norm = np.dot(np.dot(np.linalg.inv(np.dot(X_red.T, X_red)), X_red.T), y_red)

results = []
for a in rates:
    r = grad_descent(a, 101, X_red, y_red, B_init)
    results.append(r)

results.append(grad_descent(1, 51, X_red, y_red, B_init))

with open(output_filename, "w") as output:
    writer = csv.writer(output, lineterminator='\n')
    writer.writerows(results)

# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
# ax.scatter(data_red[:, 0], data_red[:, 1], y_red, zdir='z', c= 'red')
# plt.xlabel('age')
# plt.ylabel('weight')
# plt.show()
# #
