import ipdb
import pdb
import string
from itertools import product
from itertools import imap
from collections import deque


def find_neighbors(k, h):
    # rows
    l, col = k
    peers_row = set(l + str(c) for c in range(1, 10)) - set([k])
    peers_col = set(
        letter + col for letter in string.ascii_uppercase[0:9]) - set([k])

    rows = ['ABC', 'DEF', 'GHI']
    cols = ['123', '456', '789']
    box_row = [row for row in rows if l in row]
    box_col = [column for column in cols if col in column]

    box = set([r + c for r in box_row[0]
               for c in box_col[0]]) - set([k])

    return peers_row | peers_col | box

filename = 'sudokus_start.txt'
with open(filename, 'r') as f:
    sudokus = f.readlines()

s1 = sudokus[0]

# create Grid dict
letters = list(string.ascii_uppercase[0:9])
numbers = [str(k) for k in xrange(1, 10)]


# AC-3
def revise(sudoku, X_i, X_j):
    revised = False
    for x in sudoku[X_i]['domain']:
        if x in sudoku[X_j]['domain'] and len(sudoku[X_j]['domain']) == 1:
            # pdb.set_trace()  ######### Break Point ###########

            sudoku[X_i]['domain'] = sudoku[X_i]['domain'].replace(x, '')
            revised = True
    return revised


def ac3(sudoku, arcs):
    while arcs != deque():
        X_i, X_j = arcs.popleft()
        if revise(sudoku, X_i, X_j):
            # print 'Revised'
            if sudoku[X_i]['domain'] == '':
                print 'False'
                return False
            for X_k in (sudoku[X_i]['neighbors'] - set([X_j])):
                arcs.append((X_k, X_i))
    # print 'True'
    return sudoku


def solve(letters, numbers, grid):
    states = [{'init': pos, 'domain': [], 'neighbors': set()} for pos in grid]

    # Initialization
    sudoku = dict(
        zip(imap(lambda x: ''.join(x), product(letters, numbers)), states))

    for square, h in sudoku.iteritems():
        if h['init'] == '0':
            h['domain'] = '123456789'
        else:
            h['domain'] = h['init']

    for square, h in sudoku.iteritems():
        h['neighbors'] = find_neighbors(square, h)

    # Initializing arcs
    arcs = deque(((k, neighbor) for k, h in sudoku.iteritems()
                  for neighbor in h['neighbors']))
    return ac3(sudoku, arcs)


def test_complete(assignment):
    domains = [state['domain'] for (square, state) in assignment.iteritems()]
    solved = all(len(domain) == 1 for domain in domains)
    return solved


def select_var(sudoku):
    """Appply MRV to select square in sudoku"""
    domains = ((square, state['domain']) for square,
               state in sudoku.iteritems() if len(state['domain']) > 1)
    return min(domains, key=lambda x: len(x[1]))[0]


def check_consistency(square, value, sudoku):
    s_copy = sudoku.copy()
    neighbors = sudoku[square]['neighbors']
    assigned = dict((neighbor, sudoku[neighbor]['domain']) for neighbor in neighbors if len(sudoku[neighbor]['domain']) == 1)
    return not (value in assigned.values())

def prop_inference(var, sudoku):
    """Apply Forward checking for a sudoku"""
    for neighbor in sudoku[var]['neighbors']:
        sudoku[neighbor]['domain'].replace(sudoku[var]['domain'][0], '')
        if sudoku[neighbor]['domain'] == '':
            return False
    return True

def backtrack(sudoku):
    if test_complete(sudoku):
        pdb.set_trace()
        return sudoku
    var = select_var(sudoku)
    for value in sudoku[var]['domain']:
        print str((var, value)) # to see order of test in domain and var
        if check_consistency(var, value, sudoku):
            # pdb.set_trace()
            s_copy = sudoku.copy()
            s_copy[var]['domain'] = value
            # s_copy = prop_inference(var, s_copy)
            result = backtrack(s_copy)
            # pdb.set_trace()
            if test_complete(result): return result
        sudoku[var]['domain'] = sudoku[var]['domain'].replace(value, '')
    pdb.set_trace()
    print 'failure'
    return 'failure'

for i, sudoku in enumerate(sudokus):
    answer = solve(letters, numbers, sudoku)
    try:
        answer = backtrack(answer)
    except ValueError:
        ipdb.set_trace()  ######### Break Point ###########
    domains = [state['domain'] for (square, state) in answer.iteritems()]
    solved = all(len(domain) == 1 for domain in domains)
    print 'sudoku #' + str(i) + ', solved: ' + str(solved)
