import ipdb
import string
from itertools import product
from itertools import imap
from collections import deque


class Square:

    def __init__(self, key, value):
        self.key = key
        if value == '0':
            self.domain = '123456789'
        else:
            self.domain = value

    def neighbors(self):
        # row
        l, col = self.key
        peers_row = set(l + str(c) for c in range(1, 10)) - set([self.key])
        # column
        peers_column = set(
            l + col for l in string.ascii_uppercase[0:9]) - set([self.key])
        # box
        rows = ['ABC', 'DEF', 'GHI']
        cols = ['123', '456', '789']
        box_row = [row for row in rows if l in row]
        box_col = [column for column in cols if col in column]

        box = set([r + c for r in box_row[0]
                   for c in box_col[0]]) - set([self.key])
        return peers_row | peers_column | box

    def del_from_domain(self, value):
        return self.domain.replace(value, '')

    def has_empty_domain(self):
        return bool(not self.domain)


class Grid:

    def __init__(self, states):
        self.access = dict((state.key, state)for state in states)
        self.keys = self.access.keys()
        self.states = self.access.values()

    def get_square(self, key):
        return self.access[key]

    def put_square(self, square):
        self.access[square.key] = square

    def get_neighbors(self, state):
        return {self.access[key] for key in state.neighbors()}

    def get_arcs(self):
        return deque((state, self.access[key]) for state in self.states for key in state.neighbors())


filename = 'sudokus_start.txt'
with open(filename, 'r') as f:
    sudokus = f.readlines()

s1 = sudokus[0]

# create Grid dict
letters = list(string.ascii_uppercase[0:9])
numbers = [str(k) for k in xrange(1, 10)]

sudoku = dict(zip(imap(lambda x: ''.join(x), product(letters, numbers)), s1))

X = sudoku.keys()
D = set(range(0, 10))

# put all the arcs in csp into queue 81 *20
squares = [Square(position, value) for (position, value) in sudoku.iteritems()]
grid = Grid(squares)

def ac3(grid):
    arcs = grid.get_arcs()
    while arcs != deque():
        (X_i, X_j) = arcs.popleft()
        if revise(grid, X_i, X_j):
            xi = grid.get_square(X_i.key)
            if xi.has_empty_domain():
                return False
            # ipdb.set_trace()  ######### Break Point ###########

            for X_k in grid.get_neighbors(X_i) - set([X_j]):
                arcs.append((X_k, X_i))
    return True
#
#
def revise(grid, X_i, X_j):
    revised = False
    xi = grid.get_square(X_i.key)
    xj = grid.get_square(X_j.key)
    for val in xi.domain:
        if not compatible(val, xj.domain):
            xi.del_from_domain(val)
            grid.put_square(xi)
            revised = True
    return revised

def compatible(val, domain):
    return (val not in domain)


ac3(grid)
